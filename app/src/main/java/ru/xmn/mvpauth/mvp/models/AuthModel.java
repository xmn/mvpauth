package ru.xmn.mvpauth.mvp.models;

import ru.xmn.mvpauth.mvp.data.managers.DataManager;

public class AuthModel  extends AbstractModel{

    public AuthModel() {
    }

    public boolean isAuthUser() {
        return mDataManager.isUserAuth();
    }

    public void loginUser(String email, String password) {
        mDataManager.loginUser(email, password);
    }
}
