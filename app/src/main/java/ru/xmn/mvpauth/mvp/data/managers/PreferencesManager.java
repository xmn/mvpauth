package ru.xmn.mvpauth.mvp.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import ru.xmn.mvpauth.utils.MvpAuthApp;

public class PreferencesManager {
    private final SharedPreferences mSharedPreferences;
    private final String AUTH_TOKEN_KEY = "AUTH_TOKEN_KEY";

    public PreferencesManager(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void saveAuthToken(String authToken) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(AUTH_TOKEN_KEY, authToken);
        editor.apply();
    }

    public String getAuthToken() {
        return mSharedPreferences.getString(AUTH_TOKEN_KEY, "null");
    }
}
