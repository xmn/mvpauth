package ru.xmn.mvpauth.mvp.views;

public interface IAuthView extends IView {

    void showMessage(String MESSAGE_ID);

    void showLoginButton();

    void hideLoginButton();

    boolean isIdle();

    void setCustomState(int state);

    void showEmailValidation(boolean matches);

    void showPasswordValidation(boolean matches);

    String getUserEmail();

    String getUserPassword();
}
