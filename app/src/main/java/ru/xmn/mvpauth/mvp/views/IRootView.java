package ru.xmn.mvpauth.mvp.views;

import android.support.annotation.Nullable;

/**
 * Created by xmn on 09.11.2016.
 */
public interface IRootView extends IView{
    void showMessage(String message);

    void showError(Throwable error);

    void showLoad();

    void hideLoad();

    @Nullable
    IView getCurrentScreen();
}
