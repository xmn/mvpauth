package ru.xmn.mvpauth.mvp.presenters;

/**
 * Created by xmn on 29.10.2016.
 */

public interface ICatalogPresenter {
    void clickOnBuyButton(int position);
    boolean checkUserAuth();
}
