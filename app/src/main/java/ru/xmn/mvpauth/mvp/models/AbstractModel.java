package ru.xmn.mvpauth.mvp.models;

import android.provider.ContactsContract;

import javax.inject.Inject;

import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.di.components.DaggerDataManagerComponent;
import ru.xmn.mvpauth.di.components.DaggerModelComponent;
import ru.xmn.mvpauth.di.components.ModelComponent;
import ru.xmn.mvpauth.di.modules.ModelModule;
import ru.xmn.mvpauth.mvp.data.managers.DataManager;

/**
 * Created by xmn on 08.11.2016.
 */

public abstract class AbstractModel {
    @Inject
    DataManager mDataManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if (component == null) {
            component = DaggerModelComponent.builder()
                    .modelModule(new ModelModule())
                    .build();
            DaggerService.registerComponent(ModelComponent.class, component);
        }
        component.inject(this);
    }
}
