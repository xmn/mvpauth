package ru.xmn.mvpauth.mvp.presenters;

import android.util.Log;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import dagger.Provides;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.di.scopes.CatalogScope;
import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;
import ru.xmn.mvpauth.mvp.models.CatalogModel;
import ru.xmn.mvpauth.mvp.views.ICatalogView;
import ru.xmn.mvpauth.mvp.views.IRootView;
import ru.xmn.mvpauth.ui.activities.RootActivity;

/**
 * Created by xmn on 29.10.2016.
 */
public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter {
    private static final String TAG = "CatalogPresenter";
    @Inject
    RootPresenter mRootPresenter;

    @Inject
    CatalogModel mModel;
    private List<ProductDto> mProductDtoList;
    private Set<ProductDto> mBucket = new HashSet<>();

    public CatalogPresenter() {
        DaggerService.getComponent(Component.class, new Module(), DaggerService.getComponent(RootActivity.RootComponent.class))
                .inject(this);
    }

    @Override
    public void initView() {
        Log.d(TAG, "initView() called");
        if (mProductDtoList == null) {
            mProductDtoList = mModel.getProductList();
        }

        if (getView() != null) {
            getView().showCatalogView(mProductDtoList);
        }

        updateProductCounter();

    }

    IRootView getRootView(){
        return mRootPresenter.getView();
    }

    //region icatalogpresenter
    @Override
    public void clickOnBuyButton(int position) {
        if (getView() != null)
            if (checkUserAuth()) {
                addProductToBucket(position);
                getRootView().showMessage("Товар " + mProductDtoList.get(position).getProductName()
                                + " успешно добавлен в корзину");
            } else
                getView().showAuthScreen();
    }

    private void addProductToBucket(int position) {
        mBucket.add(mProductDtoList.get(position));
        updateProductCounter();
    }

    private void updateProductCounter() {
        int bucketCount = 0;
        for (ProductDto p :
                mBucket) {
            bucketCount += p.getCount();
        }
        if (getView() != null) {
            Log.d(TAG, "updateProductCounter() called");
            getView().updateProductCounter(bucketCount);
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mModel.isUserAuth();
    }
    //endregion

    //region DI
    @dagger.Module
    public class Module {
        @Provides
        @CatalogScope
        public CatalogModel provideModel() {
            return new CatalogModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @CatalogScope
    interface Component {
        void inject(CatalogPresenter presenter);
    }
    //endregion
}
