package ru.xmn.mvpauth.mvp.presenters;

import javax.inject.Inject;

import dagger.Provides;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.di.scopes.RootScope;
import ru.xmn.mvpauth.mvp.data.storage.dto.UserDto;
import ru.xmn.mvpauth.mvp.models.RootModel;
import ru.xmn.mvpauth.mvp.views.IRootView;

/**
 * Created by xmn on 09.11.2016.
 */
public class RootPresenter extends AbstractPresenter<IRootView> {
    @Inject
    RootModel mModel;

    public RootPresenter() {
        Component component = DaggerService.getComponent(RootPresenter.Component.class, new RootPresenter.Module());
        component.inject(this);
    }

    @Override
    public void initView() {
        // TODO: 09.11.2016 init user avatar + name
    }

    public UserDto getUser() {
        return mModel.getUser();
    }

    //region DI
    @dagger.Module
    public class Module {
        @Provides
        @RootScope
        public RootModel provideModel() {
            return new RootModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @RootScope
    interface Component {
        void inject(RootPresenter presenter);
    }

    //endregion
}
