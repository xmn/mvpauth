package ru.xmn.mvpauth.mvp.models;

import ru.xmn.mvpauth.mvp.data.managers.DataManager;
import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;

/**
 * Created by xmn on 28.10.2016.
 */

public class ProductModel extends AbstractModel{

    public ProductDto getProductById (int id) {
        // TODO: 28.10.2016 get prod from datamanager
        return mDataManager.getProductById(id);
    }

    public void updateProduct(ProductDto product){
        mDataManager.updateProduct(product);
    }

}
