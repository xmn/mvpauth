package ru.xmn.mvpauth.mvp.presenters;

/**
 * Created by xmn on 28.10.2016.
 */

public interface IProductPresenter {
    void clickOnPlus();
    void clickOnMinus();
}
