package ru.xmn.mvpauth.mvp.models;

import java.util.List;

import ru.xmn.mvpauth.mvp.data.managers.DataManager;
import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;

/**
 * Created by xmn on 29.10.2016.
 */

public class CatalogModel extends AbstractModel{
    public CatalogModel(){

    }
    public List<ProductDto> getProductList(){
        return mDataManager.getProductList();
    }

    public boolean isUserAuth(){
        return mDataManager.isUserAuth();
    }

}
