package ru.xmn.mvpauth.mvp.models;

import ru.xmn.mvpauth.mvp.data.storage.dto.UserDto;

/**
 * Created by xmn on 13.11.2016.
 */
public class RootModel extends AbstractModel {
    public UserDto getUser() {
        return mDataManager.getUser();
    }
}
