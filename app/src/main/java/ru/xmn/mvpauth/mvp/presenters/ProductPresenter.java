package ru.xmn.mvpauth.mvp.presenters;

import javax.inject.Inject;

import dagger.Provides;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.di.scopes.ProductScope;
import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;
import ru.xmn.mvpauth.mvp.models.ProductModel;
import ru.xmn.mvpauth.mvp.views.IProductView;

/**
 * Created by xmn on 29.10.2016.
 */
public class ProductPresenter extends AbstractPresenter<IProductView> implements IProductPresenter {
    private static final String TAG = "ProductPresenter";
    @Inject
    ProductModel mProductModel;
    private ProductDto mProduct;

    public ProductPresenter(ProductDto prod) {
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(ProductPresenter.Component.class, component);
        }
        component.inject(this);
        mProduct = prod;
    }

    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProductView(mProduct);
        }
    }

    //region iproductpresenter
    @Override
    public void clickOnPlus() {
        if (mProduct.getCount() > 0) {
            mProduct.addProduct();
            mProductModel.updateProduct(mProduct);
            if (getView() != null) {
                getView().updateProductCountView(mProduct);
            }
        }
    }

    @Override
    public void clickOnMinus() {
        if (mProduct.getCount() > 1) {
            mProduct.deleteProduct();
            mProductModel.updateProduct(mProduct);
            if (getView() != null) {
                getView().updateProductCountView(mProduct);
            }
        }
    }
    //endregion

    //region DI
    @dagger.Module
    public class Module {
        @Provides
        @ProductScope
        public ProductModel provideModel() {
            return new ProductModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @ProductScope
    interface Component {
        void inject(ProductPresenter presenter);
    }

    private Component createDaggerComponent() {
        return DaggerProductPresenter_Component.builder().module(new Module()).build();
    }
    //endregion
}
