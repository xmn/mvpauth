package ru.xmn.mvpauth.mvp.data.storage.dto;

/**
 * Created by xmn on 12.11.2016.
 */

public class UserDto {
    private String avatarUrl;
    private String name;
    private String authToken;

    public UserDto(String avatarUrl, String name) {
        this.avatarUrl = avatarUrl;
        this.name = name;
    }

    public UserDto(String avatarUrl, String name, String authToken) {
        this.avatarUrl = avatarUrl;
        this.name = name;
        this.authToken = authToken;
    }

    public String getAuthToken() {
        return authToken;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getName() {
        return name;
    }
}
