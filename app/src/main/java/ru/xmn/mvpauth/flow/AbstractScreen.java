package ru.xmn.mvpauth.flow;

import android.util.Log;

import ru.xmn.mvpauth.mortar.ScreenScoper;

import flow.ClassKey;


public abstract class AbstractScreen<T> extends ClassKey {
    private static final String TAG = "AbstractScreen";

    public String getScopeName() {
        return getClass().getName();
    }

    public abstract Object createScreenComponent(T parentComponent);

    public void unregisterScope() {
        Log.d(TAG, "unregisterScope: " + this.getScopeName());
        ScreenScoper.destroyScreenScope(getScopeName());
    }
}