package ru.xmn.mvpauth.di.components;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import ru.xmn.mvpauth.di.modules.PicassoCacheModule;
import ru.xmn.mvpauth.di.scopes.RootScope;

/**
 * Created by xmn on 08.11.2016.
 */
@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {
    Picasso getPicasso();
}
