package ru.xmn.mvpauth.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by xmn on 07.11.2016.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface RootScope {
}
