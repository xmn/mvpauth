package ru.xmn.mvpauth.di.components;

import javax.inject.Singleton;

import dagger.Component;
import ru.xmn.mvpauth.di.modules.ModelModule;
import ru.xmn.mvpauth.mvp.models.AbstractModel;

/**
 * Created by xmn on 08.11.2016.
 */

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
