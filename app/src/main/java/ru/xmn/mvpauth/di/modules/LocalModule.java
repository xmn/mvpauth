package ru.xmn.mvpauth.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.xmn.mvpauth.mvp.data.managers.PreferencesManager;

/**
 * Created by xmn on 07.11.2016.
 */

@Module
public class LocalModule {
    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context) {
        return new PreferencesManager(context);
    }
}
