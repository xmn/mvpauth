package ru.xmn.mvpauth.di;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ru.xmn.mvpauth.ui.screens.AuthScreen;

/**
 * Created by xmn on 07.11.2016.
 */

public class DaggerService {
    private static final String TAG = "DaggerService";
    public static final String SERVICE_NAME = "DAGGER_SERVICE";
    private static Map<Class, Object> sComponentMap = new HashMap<>();

    @SuppressWarnings("unchecked")
    public static <T> T getDaggerComponent(Context context) {
        return (T) context.getSystemService(SERVICE_NAME);
    }

    public static void registerComponent(Class componentClass, Object daggerComponent){
        sComponentMap.put(componentClass, daggerComponent);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public static <T> T getComponent(Class<T> componentClass) {
        Object component = sComponentMap.get(componentClass);
        return (T) component;
    }

    @SuppressWarnings("unchecked")
    public static <T> T getComponent(Class<T> componentClass, Object... dependencies) {
        Object component = sComponentMap.get(componentClass);

        if (component == null) {
            component = createComponent(componentClass, dependencies);
            registerComponent(componentClass, component);
        }

        return (T) component;
    }

    public static void unregisterScope(Class<? extends Annotation> scopeAnnotation){
        Iterator<Map.Entry<Class, Object>> iterator = sComponentMap.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<Class, Object> entry = iterator.next();
            if (entry.getKey().isAnnotationPresent(scopeAnnotation)){
                Log.e(TAG, "unregisterScope: "+ entry.getKey().getName());
                iterator.remove();
            }
        }
    }

    private static <T> T createComponent(Class<T> componentClass, Object... dependencies) {

        String fqn = componentClass.getName();

        String packageName = componentClass.getPackage().getName();
        // Accounts for inner classes, ie MyApplication$RootComponent
        String simpleName = fqn.substring(packageName.length() + 1);
        String generatedName = (packageName + ".Dagger" + simpleName).replace('$', '_');

        try {
            Class<?> generatedClass = Class.forName(generatedName);
            Object builder = generatedClass.getMethod("builder").invoke(null);

            for (Method method : builder.getClass().getMethods()) {
                Class<?>[] params = method.getParameterTypes();
                if (params.length == 1) {
                    Class<?> dependencyClass = params[0];
                    for (Object dependency : dependencies) {
                        if (dependencyClass.isAssignableFrom(dependency.getClass())) {
                            method.invoke(builder, dependency);
                            break;
                        }
                    }
                }
            }
            //noinspection unchecked
            return (T) builder.getClass().getMethod("build").invoke(builder);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
