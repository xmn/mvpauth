package ru.xmn.mvpauth.di.components;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import ru.xmn.mvpauth.di.modules.AppModule;

/**
 * Created by xmn on 07.11.2016.
 */

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
