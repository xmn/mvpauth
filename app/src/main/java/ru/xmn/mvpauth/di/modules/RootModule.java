package ru.xmn.mvpauth.di.modules;

import dagger.Provides;
import ru.xmn.mvpauth.di.scopes.RootScope;
import ru.xmn.mvpauth.mvp.presenters.RootPresenter;

/**
 * Created by xmn on 03.12.2016.
 */

@dagger.Module
public class RootModule {
    @Provides
    @RootScope
    RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }
}
