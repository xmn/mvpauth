package ru.xmn.mvpauth.di.components;

import javax.inject.Singleton;

import dagger.Component;
import ru.xmn.mvpauth.di.modules.LocalModule;
import ru.xmn.mvpauth.di.modules.NetworkModule;
import ru.xmn.mvpauth.mvp.data.managers.DataManager;

/**
 * Created by xmn on 08.11.2016.
 */

@Component(dependencies = AppComponent.class, modules = {LocalModule.class, NetworkModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
