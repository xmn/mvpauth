package ru.xmn.mvpauth.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.xmn.mvpauth.mvp.data.managers.DataManager;

/**
 * Created by xmn on 08.11.2016.
 */
@Module
public class ModelModule {
    @Singleton
    @Provides
    DataManager provideDataManager() {
        return new DataManager();
    }
}
