package ru.xmn.mvpauth.di.modules;

import android.content.Context;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.xmn.mvpauth.di.scopes.RootScope;

/**
 * Created by xmn on 07.11.2016.
 */
@Module
public class PicassoCacheModule {
    @Provides
    @RootScope
    Picasso providePicasso(Context context) {
        OkHttp3Downloader okHttp3Downloader = new OkHttp3Downloader(context);
        Picasso picasso = new Picasso.Builder(context).downloader(okHttp3Downloader).debugging(true).build();
        Picasso.setSingletonInstance(picasso);
        return picasso;
    }
}
