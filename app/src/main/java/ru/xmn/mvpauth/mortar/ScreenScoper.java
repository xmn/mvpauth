package ru.xmn.mvpauth.mortar;

import android.support.annotation.Nullable;
import android.util.Log;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import mortar.MortarScope;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.flow.AbstractScreen;

/**
 * Created by xmn on 30.11.2016.
 */
public class ScreenScoper {
    private static final String TAG = "ScreenScoper";
    private static Map<String, MortarScope> sScopeMap = new HashMap<>();
    private static final String CLASS_NAME_DIVIDER = "$";

    public static MortarScope getScreenScope(AbstractScreen screen) {
        if (!sScopeMap.containsKey(screen.getScopeName())) {
            Log.d(TAG, "getScreenScope: created new scope");
            return createScreenScope(screen);
        } else {
            Log.d(TAG, "getScreenScope: returned existing scope");
            return sScopeMap.get(screen.getScopeName());
        }
    }

    public static void registerScope(MortarScope scope) {
        sScopeMap.put(scope.getName(), scope);
    }

    private static void cleanScopeMap() {
        Iterator<Map.Entry<String, MortarScope>> iterator = sScopeMap.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, MortarScope> entry = iterator.next();

            if (entry.getValue().isDestroyed()) {
                iterator.remove();
            }
        }
    }

    public static void destroyScreenScope(String scopeName) {
        MortarScope mortarScope = sScopeMap.get(scopeName);
        mortarScope.destroy();
        cleanScopeMap();
    }

    @Nullable
    private static String getParentScopeName(AbstractScreen screen) {
        try {
            ParameterizedType type = (ParameterizedType) screen.getClass().getGenericSuperclass();
            Class screenClass = (Class) type.getActualTypeArguments()[0];
            String geneticName = screenClass.getName();

            String parentScopeName = geneticName;

            if (parentScopeName.contains(CLASS_NAME_DIVIDER)) {
                parentScopeName = parentScopeName.substring(0, geneticName.indexOf(CLASS_NAME_DIVIDER));
            }

            return parentScopeName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static MortarScope createScreenScope(AbstractScreen screen) {
        Log.d(TAG, "createScreenScope: " + screen.getScopeName());
        MortarScope parentScope = sScopeMap.get(getParentScopeName(screen));
        Object screenComponent = screen.createScreenComponent(parentScope.getService(DaggerService.SERVICE_NAME));
        MortarScope newScope = parentScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, screenComponent)
                .build(screen.getScopeName());
        registerScope(newScope);

        return newScope;
    }
}