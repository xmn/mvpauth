package ru.xmn.mvpauth.utils;

/**
 * Created by xmn on 08.11.2016.
 */
public class AppConfig {
    public static final String BASE_URL = "http://urds.ru";
    public static final int MAX_CONNECTION_TIMEOUT = 5000;
    public static final int MAX_READ_TIMEOUT = 5000;
    public static final int MAX_WRITE_TIMEOUT = 5000;
}
