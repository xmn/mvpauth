package ru.xmn.mvpauth.utils;

import android.app.Application;
import android.content.Context;

import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.di.components.AppComponent;
import ru.xmn.mvpauth.di.components.DaggerAppComponent;
import ru.xmn.mvpauth.di.modules.AppModule;
import ru.xmn.mvpauth.di.modules.PicassoCacheModule;
import ru.xmn.mvpauth.di.modules.RootModule;
import ru.xmn.mvpauth.mortar.ScreenScoper;
import ru.xmn.mvpauth.ui.activities.DaggerRootActivity_RootComponent;
import ru.xmn.mvpauth.ui.activities.RootActivity;


public class MvpAuthApp extends Application {
    public static Context sContext;
    private static AppComponent sAppComponent;
    private MortarScope mRootScope;
    private MortarScope mRootActivityScope;
    private RootActivity.RootComponent mRootActivityRootComponent;

    @Override
    public Object getSystemService(String name) {
        return mRootScope.hasService(name) ? mRootScope.getService(name) : super.getSystemService(name);
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        createAppComponent();
        createRootActivityComponent();

        sContext = getApplicationContext();

        mRootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, sAppComponent)
                .build("Root");

        mRootActivityScope = mRootScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, mRootActivityRootComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                .build(RootActivity.class.getName());

        ScreenScoper.registerScope(mRootScope);
        ScreenScoper.registerScope(mRootActivityScope);
    }

    private void createAppComponent() {
        sAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext()))
                .build();
    }

    private void createRootActivityComponent() {
        mRootActivityRootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(sAppComponent)
                .rootModule(new RootModule())
                .picassoCacheModule(new PicassoCacheModule())
                .build();
    }
}
