package ru.xmn.mvpauth.ui.screens;

import android.os.Bundle;
import android.util.Log;

import com.squareup.picasso.Picasso;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.di.scopes.CatalogScope;
import ru.xmn.mvpauth.flow.AbstractScreen;
import ru.xmn.mvpauth.flow.Screen;
import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;
import ru.xmn.mvpauth.mvp.models.CatalogModel;
import ru.xmn.mvpauth.mvp.presenters.AbstractPresenter;
import ru.xmn.mvpauth.mvp.presenters.CatalogPresenter;
import ru.xmn.mvpauth.mvp.presenters.ICatalogPresenter;
import ru.xmn.mvpauth.mvp.presenters.RootPresenter;
import ru.xmn.mvpauth.ui.activities.RootActivity;

/**
 * Created by xmn on 04.12.2016.
 */
@Screen(R.layout.screen_auth)
public class CatalogScreen extends AbstractScreen<RootActivity.RootComponent> {
    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerCatalogScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    //region DI
    @dagger.Module
    public class Module {
        @Provides
        @CatalogScope
        public CatalogModel provideModel() {
            return new CatalogModel();
        }

        @Provides
        @CatalogScope
        public CatalogPresenter providePresenter() {
            return new CatalogPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = CatalogScreen.Module.class)
    @CatalogScope
    interface Component {
        void inject(CatalogPresenter presenter);

        void inject(CatalogView view);

        CatalogModel getCatalogModel();

        Picasso getPicasso();

        RootPresenter getRootPresenter();
    }
    //endregion

    //region icatalogpresenter
    public class CatalogPresenter extends ViewPresenter<CatalogView> implements ICatalogPresenter {
        @Inject
        RootPresenter mRootPresenter;
        @Inject
        CatalogModel mCatalogModel;

        private List<ProductDto> mProductDtoList;
        private Set<ProductDto> mBucket = new HashSet<>();

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if (getView() != null) {
                getView().showCatalogView(mCatalogModel.getProductList());
            }
        }

        @Override
        public void clickOnBuyButton(int position) {
            if (getView() != null)
                if (checkUserAuth()) {
                    addProductToBucket(position);
                    mRootPresenter.getView().showMessage("Товар " + mProductDtoList.get(position).getProductName()
                            + " успешно добавлен в корзину");
                } else
                    getView().showAuthScreen();
        }

        private void addProductToBucket(int position) {
            mBucket.add(mProductDtoList.get(position));
            updateProductCounter();
        }

        private void updateProductCounter() {
            int bucketCount = 0;
            for (ProductDto p : mBucket) {
                bucketCount += p.getCount();
            }
            if (getView() != null) {
                getView().updateProductCounter(bucketCount);
            }
        }

        @Override
        public boolean checkUserAuth() {
            return mCatalogModel.isUserAuth();
        }
    }
    //endregion
}
