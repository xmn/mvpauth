package ru.xmn.mvpauth.ui.screens;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.di.scopes.AuthScope;
import ru.xmn.mvpauth.flow.AbstractScreen;
import ru.xmn.mvpauth.flow.Screen;
import ru.xmn.mvpauth.mvp.models.AuthModel;
import ru.xmn.mvpauth.mvp.presenters.IAuthPresenter;
import ru.xmn.mvpauth.mvp.presenters.RootPresenter;
import ru.xmn.mvpauth.mvp.views.IRootView;
import ru.xmn.mvpauth.ui.activities.AuthActivity;
import ru.xmn.mvpauth.ui.activities.RootActivity;

/**
 * Created by xmn on 30.11.2016.
 */
@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.RootComponent> {
    private static final String TAG = "AuthScreen";
    private int mCustomState = AuthView.IDLE_STATE;

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentRootComponent) {
        return DaggerAuthScreen_Component.builder()
                .rootComponent(parentRootComponent)
                .module(new Module())
                .build();
    }

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int customState) {
        mCustomState = customState;
    }

    //region DI
    @dagger.Module
    public class Module {
        @Provides
        @AuthScope
        AuthPresenter provideAuthPresenter() {
            return new AuthPresenter();
        }

        @Provides
        @AuthScope
        AuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @AuthScope
    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    public interface Component {
        void inject(AuthPresenter presenter);

        void inject(AuthView view);
    }
    //endregion

    //region Presenter
    public class AuthPresenter extends ViewPresenter<AuthView> implements IAuthPresenter {

        @Inject
        AuthModel mAuthModel;

        @Inject
        RootPresenter mRootPresenter;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                if (checkUserAuth()) {
                    getView().hideLoginButton();
                } else {
                    getView().showLoginButton();
                }
            }
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getView();
        }

        @Override
        public boolean checkUserAuth() {
            return mAuthModel.isAuthUser();
        }

        @Override
        public void clickOnLogin() {
        if (getView() != null) {
            if (getView().isIdle()) {
                getView().setCustomState(AuthView.LOGIN_STATE);
            } else {
                String email = getView().getUserEmail();
                String password = getView().getUserPassword();
                if (isValidEmail(email) && isValidPassword(password)) {
                    mAuthModel.loginUser(email, password);
                    getView().showMessage("Вход выполнен");
                    getView().setCustomState(AuthView.IDLE_STATE);
                } else {
                    getView().showMessage("incorrect_login_data");
                }
            }
        }
        }

        @Override
        public void clickOnFb() {
            if (getView() != null) {
                getView().showMessage("R.string.click_fb");
            }
        }

        @Override
        public void clickOnVk() {
            if (getView() != null) {
                getView().showMessage("R.string.click_vk");
            }
        }

        @Override
        public void clickOnTwitter() {
            if (getView() != null) {
                getView().showMessage("R.string.click_twitter");
            }
        }

        @Override
        public void clickOnShowCatalog() {
            Log.d(TAG, "clickOnShowCatalog() called");
            if (getView() != null && getRootView() != null) {

                if (getRootView() instanceof AuthActivity) {
                    ((AuthActivity) getRootView()).startRootActivity();
                } else {
                    // TODO: 26.11.2016 show catalog screen
                }
            }
        }



        //region check validation
        public void checkEmail(String email) {
            if (email != null && getView() != null) {
                getView().showEmailValidation(isValidEmail(email));
            }
        }

        private boolean isValidEmail(String email) {
            String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
            return email.matches(EMAIL_REGEX);
        }

        public void checkPassword(String password) {
            if (password != null && getView() != null) {
                getView().showPasswordValidation(isValidPassword(password));
            }
        }

        private boolean isValidPassword(String password) {
            int MIN_PASSWORD_LENGTH = 8;
            return password.length() >= MIN_PASSWORD_LENGTH;
        }
        //endregion
    }
    //endregion
}
