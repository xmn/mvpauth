package ru.xmn.mvpauth.ui.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.databinding.FragmentProductBinding;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.di.components.DaggerPicassoComponent;
import ru.xmn.mvpauth.di.components.PicassoComponent;
import ru.xmn.mvpauth.di.modules.PicassoCacheModule;
import ru.xmn.mvpauth.di.scopes.ProductScope;
import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;
import ru.xmn.mvpauth.mvp.presenters.ProductPresenter;
import ru.xmn.mvpauth.mvp.views.IProductView;
import ru.xmn.mvpauth.ui.activities.RootActivity;
import ru.xmn.mvpauth.utils.MvpAuthApp;

/**
 * Created by xmn on 29.10.2016.
 */

public class ProductFragment extends Fragment implements IProductView, View.OnClickListener {
    @Inject
    ProductPresenter mPresenter;
    @Inject
    Picasso mPicasso;
    FragmentProductBinding mDataBinding;

    //region new instance + bundle read
    public static ProductFragment newInstance(ProductDto product){
        Bundle bundle = new Bundle();
        bundle.putParcelable("PRODUCT", product);
        ProductFragment productFragment = new ProductFragment();
        productFragment.setArguments(bundle);
        return productFragment;
    }

    private void readBundle(Bundle bundle){
        if (bundle != null) {
            ProductDto product = bundle.getParcelable("PRODUCT");
            Component component =createDaggerComponent(product);
            component.inject(this);
            // TODO: 08.11.2016 fix recreate component
        }
    }
    //endregion

    //region lifecycle
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mDataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product, null, false);
        View view = mDataBinding.getRoot();
        ButterKnife.bind(this, view);
        readBundle(getArguments());
        mPresenter.takeView(this);
        mPresenter.initView();
        mDataBinding.plusBtn.setOnClickListener(this);
        mDataBinding.minusBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }
    //endregion

    //region iproductview
    @Override
    public void showProductView(ProductDto product) {
        mDataBinding.setProduct(product);
        mDataBinding.setProductImage(product.getImageUrl());
    }

    @Override
    public void updateProductCountView(ProductDto product) {
        mDataBinding.setProduct(product);
    }
    //endregion

    //region Click
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.plus_btn:
                mPresenter.clickOnPlus();
                break;
            case R.id.minus_btn:
                mPresenter.clickOnMinus();
                break;
        }
    }
    //endregion

    //region DI
    private Component createDaggerComponent(ProductDto productDto) {
        PicassoComponent picassoComponent = DaggerService.getComponent(PicassoComponent.class);
        if (picassoComponent == null) {
            picassoComponent = DaggerPicassoComponent.builder()
                    .appComponent(MvpAuthApp.getAppComponent())
                    .picassoCacheModule(new PicassoCacheModule())
                    .build();
            DaggerService.registerComponent(PicassoComponent.class, picassoComponent);
        }

        return DaggerProductFragment_Component.builder()
                .picassoComponent(picassoComponent)
                .module(new Module(productDto))
                .build();
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @dagger.Module
    public class Module {
        ProductDto mProductDto;

        public Module(ProductDto productDto) {
            mProductDto = productDto;
        }

        @Provides
        @ProductScope
        ProductPresenter providePresenter(){
            return new ProductPresenter(mProductDto);
        }
    }

    @dagger.Component(dependencies = PicassoComponent.class, modules = Module.class)
    @ProductScope
    interface Component {
        void inject (ProductFragment fragment);
    }
    //endregion

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

}
