package ru.xmn.mvpauth.ui.ui_utils;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import ru.xmn.mvpauth.R;

/**
 * Created by xmn on 01.11.2016.
 */

public final class DataBinder {

    private DataBinder() {
        //NO-OP
    }

    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Context context = imageView.getContext();
        Glide.with(context).load(url).into(imageView);
    }
    @BindingAdapter("imageUrlCircle")
    public static void setImageUrlCircle(ImageView imageView, String url) {
        Context context = imageView.getContext();
        Glide.with(context).load(R.drawable.boyar_img).asBitmap().centerCrop()
                .transform(new CropCircleTransformation(context)).into(imageView);
    }
}