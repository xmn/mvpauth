package ru.xmn.mvpauth.ui.ui_utils;

/**
 * Created by xmn on 13.11.2016.
 */

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

public class AvatarImageBehavior extends CoordinatorLayout.Behavior<ImageView> {

    private final static String TAG = "behavior";
    private Context mContext;

    public AvatarImageBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, ImageView child, View dependency) {
        return dependency instanceof AppBarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, ImageView child, View dependency) {
        if (child.getVisibility()==View.VISIBLE) {
            int maxAblHeight = ((AppBarLayout) dependency).getTotalScrollRange();
            int curAblHeight = dependency.getBottom() - dependency.getHeight() + ((AppBarLayout) dependency).getTotalScrollRange();
            float sc = (float) curAblHeight / maxAblHeight;
            DecelerateInterpolator interpolator = new DecelerateInterpolator();
            float interpolatedScale = interpolator.getInterpolation(sc);
            child.setScaleX(interpolatedScale);
            child.setScaleY(interpolatedScale);
            child.setY(dependency.getBottom()-child.getHeight()/2);
        }
        return true;
    }
}
