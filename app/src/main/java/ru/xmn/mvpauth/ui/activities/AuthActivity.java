package ru.xmn.mvpauth.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import javax.inject.Inject;

import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import ru.xmn.mvpauth.BuildConfig;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.flow.TreeKeyDispatcher;
import ru.xmn.mvpauth.mortar.ScreenScoper;
import ru.xmn.mvpauth.mvp.presenters.RootPresenter;
import ru.xmn.mvpauth.mvp.views.IRootView;
import ru.xmn.mvpauth.mvp.views.IView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.xmn.mvpauth.ui.screens.AuthScreen;

public class AuthActivity extends AppCompatActivity implements IRootView {
    private static final String TAG = "AuthActivity";

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;

    @Inject
    RootPresenter mRootPresenter;

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new AuthScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    //region activity
    @Override
    protected void onResume() {
        mRootPresenter.takeView(this);
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        DaggerService.<RootActivity.RootComponent>getDaggerComponent(this).inject(this);
    }

    @Override
    protected void onPause() {
        mRootPresenter.dropView();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            ScreenScoper.destroyScreenScope(AuthScreen.class.getName());
        }
        super.onDestroy();
    }

    @Override
    public void showError(Throwable error) {
        if (BuildConfig.DEBUG) {
            showMessage(error.getMessage());
            error.printStackTrace();
        } else {
            showMessage("FATAL ERROR");
            // TODO: 20.10.2016 sent to crashlytics
        }
    }
    //endregion

    //region iview
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLoad() {
//        if (mProgressBar.getVisibility() == View.GONE)
//            mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoad() {
//        if (mProgressBar.getVisibility() == View.VISIBLE)
//            mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Выход")
                .setMessage("Вы уверены что хотите закрыть приложение?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, (arg0, arg1) -> AuthActivity.super.onBackPressed())
                .create().show();
    }

    //endregion

    public void startRootActivity() {
        startActivity(new Intent(this, RootActivity.class));
        finish();
    }

    @Override
    public boolean viewOnBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            super.onBackPressed();
        }
        return false;
    }
}
