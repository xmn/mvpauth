package ru.xmn.mvpauth.ui.custom_views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


public class AuthTitleView extends TextView {
    private final String CUSTOM_FONT = "fonts/PTBebasNeueBook.ttf";

    public AuthTitleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),CUSTOM_FONT));
    }

}
