package ru.xmn.mvpauth.ui.screens;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.rd.PageIndicatorView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;
import ru.xmn.mvpauth.mvp.presenters.CatalogPresenter;
import ru.xmn.mvpauth.mvp.views.ICatalogView;
import ru.xmn.mvpauth.ui.fragments.adapters.CatalogAdapter;

/**
 * Created by xmn on 04.12.2016.
 */
public class CatalogView extends RelativeLayout implements ICatalogView {

    @Inject
    private CatalogScreen.CatalogPresenter mPresenter;
    private CatalogScreen mScreen;

    @BindView(R.id.add_to_card_btn)
    Button addToCardBtn;
    @BindView(R.id.product_pager)
    ViewPager productPager;
    @BindView(R.id.pageIndicatorView)
    PageIndicatorView pageIndicatorView;

    public CatalogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
            DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region lifecycle
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        addToCardBtn.setOnClickListener(view -> mPresenter.clickOnBuyButton(productPager.getCurrentItem()));
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }
    //endregion

    //region icatalogview
    @Override
    public void showCatalogView(List<ProductDto> productDtoList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());
        for (ProductDto product :
                productDtoList) {
            adapter.addItem(product);
        }

        pageIndicatorView.setViewPager(productPager);
        pageIndicatorView.setCount(productDtoList.size());
        productPager.setAdapter(adapter);
    }

    @Override
    public void showAuthScreen() {
//        AuthFragment authFragment = new AuthFragment();
//        authFragment.setParentId(AuthFragment.PARENT_ROOT);
//        Log.d(TAG, "showAuthScreen() called " + authFragment);
//        getRootActivity().replaceFragment(authFragment);
    }

    @Override
    public void updateProductCounter(int count) {
        getRootActivity().updateProductCounter(count);
    }
    //endregion

}
