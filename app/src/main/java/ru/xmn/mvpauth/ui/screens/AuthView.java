package ru.xmn.mvpauth.ui.screens;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.mvp.presenters.IAuthPresenter;
import ru.xmn.mvpauth.mvp.views.IAuthView;
import ru.xmn.mvpauth.mvp.views.IRootView;
import ru.xmn.mvpauth.ui.activities.RootActivity;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by xmn on 30.11.2016.
 */
public class AuthView extends RelativeLayout implements IAuthView {
    private static final String TAG = "AuthView";
    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;

    @Inject
    AuthScreen.AuthPresenter mPresenter;
    private AuthScreen mScreen;

    //region butterknife
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    @BindView(R.id.auth_card)
    CardView mAuthCard;
    @BindView(R.id.vk_btn)
    ImageButton mVkButton;
    @BindView(R.id.fb_btn)
    ImageButton mFbButton;
    @BindView(R.id.twitter_btn)
    ImageButton mTwitterButton;
    @BindView(R.id.login_email_et)
    TextInputEditText mEmailEditText;
    @BindView(R.id.login_password_et)
    TextInputEditText mPasswordEditText;
    @BindView(R.id.login_email_wrap)
    TextInputLayout mEmailFieldWrapper;
    @BindView(R.id.login_password_wrap)
    TextInputLayout mPasswordFieldWrapper;
    //endregion

    public AuthView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
            DaggerService.<AuthScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region lifecycle
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

        bindRx();

        showViewFromState();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }
    //endregion

    //region authview
    @Override
    public void showEmailValidation(boolean matches) {
        if (matches) {
            mEmailFieldWrapper.setErrorEnabled(false);
        } else {
            mEmailFieldWrapper.setError(getResources().getString(R.string.incorrect_email));
        }
    }

    @Override
    public void showPasswordValidation(boolean matches) {
        if (matches) {
            mPasswordFieldWrapper.setErrorEnabled(false);
        } else {
            mPasswordFieldWrapper.setError(getResources().getString(R.string.incorrect_password_length));
        }
    }

    @Override
    public String getUserEmail() {
        return String.valueOf(mEmailEditText.getText());
    }

    @Override
    public String getUserPassword() {
        return String.valueOf(mPasswordEditText.getText());
    }

    public void showMessage(String message) {
//        IRootView iView = (IRootView) mActivity;
//        iView.showMessage(message);
    }

    @Override
    public void showLoginButton() {
        mLoginBtn.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoginButton() {
        mLoginBtn.setVisibility(GONE);
    }


    @Override
    public boolean viewOnBackPressed() {
        if (!isIdle()) {
            setCustomState(IDLE_STATE);
            return true;
        } else {
            return false;
        }
    }
    //endregion

    //region Events

    @OnClick(R.id.login_btn)
    void loginClick() {
        mPresenter.clickOnLogin();
    }

    @OnClick(R.id.show_catalog_btn)
    void showCatalogClick() {
        mPresenter.clickOnShowCatalog();
    }

    @OnClick(R.id.vk_btn)
    void loginVkClick() {
        mPresenter.clickOnVk();
    }

    @OnClick(R.id.fb_btn)
    void loginFbClick() {
        mPresenter.clickOnFb();
    }

    @OnClick(R.id.twitter_btn)
    void loginTwitterClick() {
        mPresenter.clickOnTwitter();
    }

    //endregion

    //region bind fields rx
    private void bindRx() {
        Observable<String> emailObservable = RxTextView.textChanges(mEmailEditText).compose(transformValidationCharSeq);
        Observable<String> passwordObservable = RxTextView.textChanges(mPasswordEditText).compose(transformValidationCharSeq);

        emailObservable.subscribe(mPresenter::checkEmail);
        passwordObservable.subscribe(mPresenter::checkPassword);
    }

    Observable.Transformer<CharSequence, String> transformValidationCharSeq = obs -> obs
            .debounce(500, TimeUnit.MILLISECONDS)
            .map(CharSequence::toString)
            .observeOn(AndroidSchedulers.mainThread())
            .filter(s -> s.length() > 0);
    //endregion

    //region state
    @Override
    public void setCustomState(int state) {
        mScreen.setCustomState(state);
        showViewFromState();
    }

    public boolean isIdle() {
        return mScreen.getCustomState() == IDLE_STATE;
    }

    private void showViewFromState() {
        if (mScreen.getCustomState() == LOGIN_STATE) {
            showLoginState();
        } else {
            showIdleState();
        }
    }

    private void showLoginState() {
        mAuthCard.setVisibility(VISIBLE);
        mShowCatalogBtn.setVisibility(GONE);
    }

    private void showIdleState() {
        mAuthCard.setVisibility(GONE);
        mShowCatalogBtn.setVisibility(VISIBLE);
    }
    //endregion
}
