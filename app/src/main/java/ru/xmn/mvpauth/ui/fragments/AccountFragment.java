package ru.xmn.mvpauth.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.ui.activities.RootActivity;

public class AccountFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getRootActivity().setCheckedItem(R.id.nav_account);
        getRootActivity().syncFrags();
    }

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }
}
