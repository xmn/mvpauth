package ru.xmn.mvpauth.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rd.PageIndicatorView;
import com.rd.animation.AnimationType;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.di.scopes.CatalogScope;
import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;
import ru.xmn.mvpauth.mvp.presenters.CatalogPresenter;
import ru.xmn.mvpauth.mvp.views.ICatalogView;
import ru.xmn.mvpauth.ui.activities.RootActivity;
import ru.xmn.mvpauth.ui.fragments.adapters.CatalogAdapter;

/**
 * Created by xmn on 29.10.2016.
 */

public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener {
    private static final String TAG = "CatalogFragment";
    @Inject
    CatalogPresenter mPresenter;
    @BindView(R.id.add_to_card_btn)
    Button addToCardBtn;
    @BindView(R.id.product_pager)
    ViewPager productPager;
    @BindView(R.id.pageIndicatorView)
    PageIndicatorView pageIndicatorView;

    public CatalogFragment(){

    }

    //region lifecycle
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        ButterKnife.bind(this, view);
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);
        mPresenter.takeView(this);
        mPresenter.initView();
        addToCardBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getRootActivity().setCheckedItem(R.id.nav_catalog);
        getRootActivity().syncFrags();
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }
    //endregion

    //region icatalogview
    @Override
    public void showCatalogView(List<ProductDto> productDtoList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());
        for (ProductDto product :
                productDtoList) {
            adapter.addItem(product);
        }

        pageIndicatorView.setViewPager(productPager);
        pageIndicatorView.setCount(productDtoList.size());
        productPager.setAdapter(adapter);
    }

    @Override
    public void showAuthScreen() {
//        AuthFragment authFragment = new AuthFragment();
//        authFragment.setParentId(AuthFragment.PARENT_ROOT);
//        Log.d(TAG, "showAuthScreen() called " + authFragment);
//        getRootActivity().replaceFragment(authFragment);
    }

    @Override
    public void updateProductCounter(int count) {
        getRootActivity().updateProductCounter(count);
    }
    //endregion

    RootActivity getRootActivity () {
        return (RootActivity) getActivity();
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.add_to_card_btn) {
            mPresenter.clickOnBuyButton(productPager.getCurrentItem());
        }
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //region DI
    @dagger.Module
    public class Module {
        @Provides
        @CatalogScope
        CatalogPresenter provideCatalogPresenter(){
            return new CatalogPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @CatalogScope
    interface Component {
        void inject (CatalogFragment fragment);
    }

    private Component createDaggerComponent() {
        return DaggerCatalogFragment_Component.builder().module(new Module()).build();
    }
    //endregion
}
