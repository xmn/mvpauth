package ru.xmn.mvpauth.ui.activities;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.databinding.ActivityRootBinding;
import ru.xmn.mvpauth.di.DaggerService;
import ru.xmn.mvpauth.di.components.AppComponent;
import ru.xmn.mvpauth.di.modules.PicassoCacheModule;
import ru.xmn.mvpauth.di.modules.RootModule;
import ru.xmn.mvpauth.di.scopes.RootScope;
import ru.xmn.mvpauth.flow.TreeKeyDispatcher;
import ru.xmn.mvpauth.mvp.data.storage.dto.UserDto;
import ru.xmn.mvpauth.mvp.presenters.RootPresenter;
import ru.xmn.mvpauth.mvp.views.IRootView;
import ru.xmn.mvpauth.mvp.views.IView;
import ru.xmn.mvpauth.ui.fragments.AccountFragment;
import ru.xmn.mvpauth.ui.fragments.CatalogFragment;
import ru.xmn.mvpauth.ui.screens.AuthScreen;
import ru.xmn.mvpauth.ui.screens.CatalogScreen;

public class RootActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, IRootView {
    private static final String TAG = "RootActivity";
    @Inject
    RootPresenter mRootPresenter;
    private ActivityRootBinding mBinding;
    FragmentManager mFragmentManager;
    private TextView mBucketCountView;

    private int mBucketInt = 0;

    private UserDto mUser;

    //region IView
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(Throwable error) {

    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return null;
    }

    //endregion

    //region activity
    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new AuthScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_root);

        ButterKnife.bind(this);
        initToolbar();
        initDrawer();

        RootComponent rootComponent = DaggerService.getDaggerComponent(this);
        rootComponent.inject(this);

        mUser = mRootPresenter.getUser();

        mRootPresenter.takeView(this);
        mRootPresenter.initView();

        mFragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new CatalogFragment())
                    .commit();
        }
    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        MenuItem item = menu.findItem(R.id.badge);
        MenuItemCompat.setActionView(item, R.layout.bucket_layout);
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(item);

        mBucketCountView = (TextView) notifCount.findViewById(R.id.actionbar_notifcation_textview);
        updateProductCounter(mBucketInt);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.nav_account:
                fragment = new AccountFragment();
                break;
            case R.id.nav_catalog:
                fragment = new CatalogFragment();
                break;
            case R.id.nav_favorite:
                fragment = new AccountFragment();
                break;
            case R.id.nav_orders:
                fragment = new AccountFragment();
                break;
            case R.id.nav_notification:
                fragment = new AccountFragment();
                break;
        }
        if (fragment != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
            item.setChecked(true);
        }
        mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
            syncFrags();
    }

    private void initDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mBinding.drawerLayout, mBinding.toolbar, R.string.open_drawer, R.string.close_drawer);
        mBinding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        mBinding.navView.setNavigationItemSelectedListener(this);
        mBinding.setUser(mUser);
    }

    private void initToolbar() {
        setSupportActionBar(mBinding.toolbar);
    }

    public void updateProductCounter(int count) {
        Log.d(TAG, "updateProductCounter() called with: count = [" + count + "]");
        if (mBucketCountView != null) {
            if (count > 0) {
                mBucketCountView.setVisibility(View.VISIBLE);
                mBucketCountView.setText(String.valueOf(count));
            } else {
                mBucketCountView.setVisibility(View.GONE);
            }
        } else mBucketInt = count;

    }

    //endregion

    //region fragments
    public void syncFrags() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof AccountFragment) {
            enableCollapse();
        } else  {
            disableCollapse();
        }
    }

    private void disableCollapse() {
        mBinding.headerBg.setVisibility(View.GONE);
        mBinding.userAvatar.setVisibility(View.GONE);
        mBinding.collapsingToolbar.setTitleEnabled(false);
        mBinding.toolbar.setTitle(getString(R.string.app_name));
    }

    private void enableCollapse() {
        mBinding.headerBg.setVisibility(View.VISIBLE);
        mBinding.userAvatar.setVisibility(View.VISIBLE);
        mBinding.collapsingToolbar.setTitleEnabled(true);
        mBinding.toolbar.setTitle(mUser.getName());
    }

    public void replaceFragment(Fragment fragment) {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    public void setCheckedItem(int checkedItem) {
        mBinding.navView.getMenu().findItem(checkedItem).setChecked(true);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }
    //endregion


    //region DI

    @dagger.Component(dependencies = AppComponent.class, modules = {RootModule.class, PicassoCacheModule.class})
    @RootScope
    public interface RootComponent {
        void inject(RootActivity activity);

        void inject(AuthActivity activity);

        Picasso getPicasso();

        RootPresenter getRootPresenter();
    }

    //endregion
}
